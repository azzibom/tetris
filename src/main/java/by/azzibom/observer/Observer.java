package by.azzibom.observer;

public interface Observer<T> {

    void update(T arg);
}
